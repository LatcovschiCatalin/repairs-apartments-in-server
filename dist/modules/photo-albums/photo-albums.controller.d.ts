import { PhotoAlbumsService } from './photo-albums.service';
import { CreatePhotoAlbumDto } from './dto/create-photo-album.dto';
import { PhotoAlbumsSchema } from "../../schemas/photo-albums.schema";
export declare class PhotoAlbumsController {
    private readonly service;
    constructor(service: PhotoAlbumsService);
    findAll(query: any): Promise<PhotoAlbumsSchema[]>;
    findById(id: string): Promise<PhotoAlbumsSchema>;
    create(createProductDto: CreatePhotoAlbumDto): Promise<PhotoAlbumsSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<PhotoAlbumsSchema>;
    remove(id: string): Promise<PhotoAlbumsSchema>;
}
