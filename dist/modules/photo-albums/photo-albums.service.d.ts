import { CreatePhotoAlbumDto } from './dto/create-photo-album.dto';
import { PaginateModel } from "mongoose-paginate-v2";
import { PhotoAlbumsSchema } from "../../schemas/photo-albums.schema";
export declare class PhotoAlbumsService {
    private model;
    constructor(model: PaginateModel<PhotoAlbumsSchema>);
    findById(id: string): Promise<PhotoAlbumsSchema>;
    create(createProductDto: CreatePhotoAlbumDto): Promise<PhotoAlbumsSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<PhotoAlbumsSchema>;
    findAll(queryParams: any): Promise<PhotoAlbumsSchema[]>;
    remove(id: string): Promise<PhotoAlbumsSchema>;
}
