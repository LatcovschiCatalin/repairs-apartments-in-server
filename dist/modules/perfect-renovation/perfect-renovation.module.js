"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PerfectRenovationModule = void 0;
const common_1 = require("@nestjs/common");
const perfect_renovation_service_1 = require("./perfect-renovation.service");
const perfect_renovation_controller_1 = require("./perfect-renovation.controller");
const mongoose_1 = require("@nestjs/mongoose");
const perfect_renovation_schema_1 = require("../../schemas/perfect-renovation.schema");
let PerfectRenovationModule = class PerfectRenovationModule {
};
PerfectRenovationModule = __decorate([
    common_1.Module({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: 'PerfectRenovation', schema: perfect_renovation_schema_1.PerfectRenovationSchema }])],
        controllers: [perfect_renovation_controller_1.PerfectRenovationController],
        providers: [perfect_renovation_service_1.PerfectRenovationService]
    })
], PerfectRenovationModule);
exports.PerfectRenovationModule = PerfectRenovationModule;
//# sourceMappingURL=perfect-renovation.module.js.map