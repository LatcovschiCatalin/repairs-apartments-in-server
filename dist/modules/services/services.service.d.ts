import { CreateServiceDto } from './dto/create-service.dto';
import { PaginateModel } from 'mongoose-paginate-v2';
import { ServicesSchema } from "../../schemas/services.schema";
export declare class ServicesService {
    private model;
    constructor(model: PaginateModel<ServicesSchema>);
    findById(id: string): Promise<ServicesSchema>;
    create(createProductDto: CreateServiceDto): Promise<ServicesSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<ServicesSchema>;
    findAll(queryParams: any): Promise<ServicesSchema[]>;
    remove(id: string): Promise<ServicesSchema>;
}
