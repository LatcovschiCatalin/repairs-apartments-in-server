"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyService = void 0;
const common_1 = require("@nestjs/common");
const mongoose_1 = require("@nestjs/mongoose");
const mongoose_paginate_v2_1 = require("mongoose-paginate-v2");
let CompanyService = class CompanyService {
    constructor(model) {
        this.model = model;
    }
    async findById(id) {
        return await this.model.findById(id).lean();
    }
    async create(createProductDto) {
        return this.model.create(createProductDto);
    }
    async findByIdAndUpdate(id, product) {
        return this.model.findByIdAndUpdate(id, product);
    }
    async findAll(queryParams) {
        const query = {};
        const options = {
            lean: true,
            page: parseInt(queryParams.page, 10) || 1,
            limit: parseInt(queryParams.limit, 10) || 10,
            sort: { createdAt: -1 },
        };
        return this.model.paginate(query, options);
    }
    async remove(id) {
        return this.model.findByIdAndDelete(id);
    }
};
CompanyService = __decorate([
    common_1.Injectable(),
    __param(0, mongoose_1.InjectModel('Company')),
    __metadata("design:paramtypes", [typeof (_a = typeof mongoose_paginate_v2_1.PaginateModel !== "undefined" && mongoose_paginate_v2_1.PaginateModel) === "function" ? _a : Object])
], CompanyService);
exports.CompanyService = CompanyService;
//# sourceMappingURL=company.service.js.map