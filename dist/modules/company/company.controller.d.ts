import { CompanyService } from './company.service';
import { CreateCompanyDto } from './dto/create-company.dto';
import { CompanySchema } from '../../schemas/company.schema';
export declare class CompanyController {
    private readonly service;
    constructor(service: CompanyService);
    findAll(query: any): Promise<CompanySchema[]>;
    findById(id: string): Promise<CompanySchema>;
    create(createProductDto: CreateCompanyDto): Promise<CompanySchema>;
    findByIdAndUpdate(id: string, product: any): Promise<CompanySchema>;
    remove(id: string): Promise<CompanySchema>;
}
