import { CreateCompanyDto } from './dto/create-company.dto';
import { CompanySchema } from "../../schemas/company.schema";
import { PaginateModel } from 'mongoose-paginate-v2';
export declare class CompanyService {
    private model;
    constructor(model: PaginateModel<CompanySchema>);
    findById(id: string): Promise<CompanySchema>;
    create(createProductDto: CreateCompanyDto): Promise<CompanySchema>;
    findByIdAndUpdate(id: string, product: any): Promise<CompanySchema>;
    findAll(queryParams: any): Promise<CompanySchema[]>;
    remove(id: string): Promise<CompanySchema>;
}
