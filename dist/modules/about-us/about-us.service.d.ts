import { CreateAboutUsDto } from './dto/create-about-us.dto';
import { AboutUsSchema } from "../../schemas/about-us.schema";
import { PaginateModel } from 'mongoose-paginate-v2';
export declare class AboutUsService {
    private model;
    constructor(model: PaginateModel<AboutUsSchema>);
    findById(id: string): Promise<AboutUsSchema>;
    create(createProductDto: CreateAboutUsDto): Promise<AboutUsSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<AboutUsSchema>;
    findAll(queryParams: any): Promise<AboutUsSchema[]>;
    remove(id: string): Promise<AboutUsSchema>;
}
