"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AboutUsController = void 0;
const common_1 = require("@nestjs/common");
const about_us_service_1 = require("./about-us.service");
const create_about_us_dto_1 = require("./dto/create-about-us.dto");
const swagger_1 = require("@nestjs/swagger");
const passport_1 = require("@nestjs/passport");
const roles_decorator_1 = require("../../decorators/roles.decorator");
let AboutUsController = class AboutUsController {
    constructor(service) {
        this.service = service;
    }
    async findAll(query) {
        return this.service.findAll(query);
    }
    async findById(id) {
        return this.service.findById(id);
    }
    async create(createProductDto) {
        return this.service.create(createProductDto);
    }
    async findByIdAndUpdate(id, product) {
        return this.service.findByIdAndUpdate(id, product);
    }
    async remove(id) {
        return this.service.remove(id);
    }
};
__decorate([
    common_1.Get(),
    __param(0, common_1.Query()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], AboutUsController.prototype, "findAll", null);
__decorate([
    common_1.Get('/:id'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AboutUsController.prototype, "findById", null);
__decorate([
    swagger_1.ApiBearerAuth(),
    common_1.Post(),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    roles_decorator_1.Roles('admin'),
    __param(0, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [create_about_us_dto_1.CreateAboutUsDto]),
    __metadata("design:returntype", Promise)
], AboutUsController.prototype, "create", null);
__decorate([
    swagger_1.ApiBearerAuth(),
    common_1.Put(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    roles_decorator_1.Roles('admin'),
    __param(0, common_1.Param('id')),
    __param(1, common_1.Body()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String, Object]),
    __metadata("design:returntype", Promise)
], AboutUsController.prototype, "findByIdAndUpdate", null);
__decorate([
    swagger_1.ApiBearerAuth(),
    common_1.Delete(':id'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    roles_decorator_1.Roles('admin'),
    __param(0, common_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [String]),
    __metadata("design:returntype", Promise)
], AboutUsController.prototype, "remove", null);
AboutUsController = __decorate([
    swagger_1.ApiTags('AboutUs'),
    common_1.Controller('about-us'),
    __metadata("design:paramtypes", [about_us_service_1.AboutUsService])
], AboutUsController);
exports.AboutUsController = AboutUsController;
//# sourceMappingURL=about-us.controller.js.map