export declare class CreateUserDto {
    readonly firstName: string;
    readonly lastName: string;
    readonly role: string;
    readonly description: string;
    readonly isActive: boolean;
    readonly email: string;
    readonly password: string;
    activationToken: string;
}
