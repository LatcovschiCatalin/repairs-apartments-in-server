import { CreateUserDto } from './dto/create-user.dto';
import { PaginateModel } from 'mongoose-paginate-v2';
import { User } from '../../schemas/user.schema';
import { UpdateUserDto } from "./dto/update-user.dto";
import { EmailEvents } from '../email/email.events';
export declare class UsersService {
    private userModel;
    private readonly emitter;
    constructor(userModel: PaginateModel<User>, emitter: EmailEvents);
    getMyProfile(id: string): Promise<User>;
    findAll(queryParams: any): Promise<User[]>;
    findById(id: string): Promise<User>;
    findOneByEmail(email: string): Promise<User>;
    create(user: CreateUserDto): Promise<User>;
    findByIdAndUpdate(id: string, userData: UpdateUserDto): Promise<User>;
    findByIdAndDelete(id: string): Promise<User>;
    findOneByKeyForPasswordReset(key: any): Promise<any>;
    findOneByEmailForActivation(email: any, activationToken: any): Promise<any>;
    activateUser(id: any): Promise<any>;
    generateToken: () => string;
}
