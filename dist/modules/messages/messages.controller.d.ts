import { MessagesService } from './messages.service';
import { CreateMessageDto } from './dto/create-message.dto';
import { MessagesSchema } from "../../schemas/messages.schema";
import { EmailService } from "../email/email.service";
export declare class MessagesController {
    private readonly service;
    private readonly email;
    constructor(service: MessagesService, email: EmailService);
    findAll(query: any): Promise<MessagesSchema[]>;
    findAllMessages(query: any): Promise<MessagesSchema[]>;
    findById(id: string): Promise<MessagesSchema>;
    create(createProductDto: CreateMessageDto): Promise<MessagesSchema>;
    sendEmail(createProductDto: CreateMessageDto): Promise<void>;
    findByIdAndUpdate(id: string, product: any): Promise<MessagesSchema>;
    remove(id: string): Promise<MessagesSchema>;
    removeEmail(id: string): Promise<MessagesSchema>;
}
