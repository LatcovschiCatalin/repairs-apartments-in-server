export declare class BcryptService {
    comparePassword(password: string | undefined, hash: string | undefined): Promise<boolean>;
}
