import { AuthService } from './auth.service';
import { UsersService } from '../users/users.service';
import { BcryptService } from './bcrypt.service';
import { EmailEvents } from '../email/email.events';
import { CreateAuthDto } from "./dto/create-auth.dto";
import { ErrorsService } from '../../config/errors.service';
export declare class AuthController {
    private readonly emitter;
    private authService;
    private userService;
    private bcryptService;
    private error;
    constructor(emitter: EmailEvents, authService: AuthService, userService: UsersService, bcryptService: BcryptService, error: ErrorsService);
    authenticateDashboard(res: any, body: CreateAuthDto): Promise<any>;
}
