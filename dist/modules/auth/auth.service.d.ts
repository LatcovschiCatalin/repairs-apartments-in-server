import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../users/users.service';
import { JwtPayload } from './interfaces/jwt-payload.interface';
import { ErrorsService } from '../../config/errors.service';
export declare class AuthService {
    private readonly usersService;
    private readonly jwtService;
    private error;
    constructor(usersService: UsersService, jwtService: JwtService, error: ErrorsService);
    create(user: any): Promise<any>;
    validateUser(payload: JwtPayload): Promise<any>;
    validateUserById(userId: string): Promise<any>;
    signIn(user: any): Promise<string>;
    generateJWTToken(user: any): string;
    decode(token: string): any;
}
export declare const generateToken: () => string;
