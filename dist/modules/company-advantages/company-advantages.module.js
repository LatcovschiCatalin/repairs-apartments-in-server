"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyAdvantagesModule = void 0;
const common_1 = require("@nestjs/common");
const company_advantages_service_1 = require("./company-advantages.service");
const company_advantages_controller_1 = require("./company-advantages.controller");
const mongoose_1 = require("@nestjs/mongoose");
const company_advantages_schema_1 = require("../../schemas/company-advantages.schema");
const upload_controller_1 = require("../upload/upload.controller");
const upload_service_1 = require("../upload/upload.service");
let CompanyAdvantagesModule = class CompanyAdvantagesModule {
};
CompanyAdvantagesModule = __decorate([
    common_1.Module({
        imports: [mongoose_1.MongooseModule.forFeature([{ name: 'CompanyAdvantages', schema: company_advantages_schema_1.CompanyAdvantagesSchema }])],
        controllers: [company_advantages_controller_1.CompanyAdvantagesController, upload_controller_1.UploadController],
        providers: [company_advantages_service_1.CompanyAdvantagesService, upload_service_1.UploadService]
    })
], CompanyAdvantagesModule);
exports.CompanyAdvantagesModule = CompanyAdvantagesModule;
//# sourceMappingURL=company-advantages.module.js.map