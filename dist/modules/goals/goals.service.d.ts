import { CreateGoalDto } from './dto/create-goal.dto';
import { GoalsSchema } from "../../schemas/goals.schema";
import { PaginateModel } from "mongoose-paginate-v2";
export declare class GoalsService {
    private model;
    constructor(model: PaginateModel<GoalsSchema>);
    findById(id: string): Promise<GoalsSchema>;
    create(createProductDto: CreateGoalDto): Promise<GoalsSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<GoalsSchema>;
    findAll(queryParams: any): Promise<GoalsSchema[]>;
    remove(id: string): Promise<GoalsSchema>;
}
