import { OrganizationService } from './organization.service';
import { CreateOrganizationDto } from './dto/create-organization.dto';
import { OrganizationSchema } from "../../schemas/organization.schema";
export declare class OrganizationController {
    private readonly service;
    constructor(service: OrganizationService);
    findAll(query: any): Promise<OrganizationSchema[]>;
    findById(id: string): Promise<OrganizationSchema>;
    create(createProductDto: CreateOrganizationDto): Promise<OrganizationSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<OrganizationSchema>;
    remove(id: string): Promise<OrganizationSchema>;
}
