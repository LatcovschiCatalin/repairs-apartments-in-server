"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadController = void 0;
const common_1 = require("@nestjs/common");
const platform_express_1 = require("@nestjs/platform-express");
const multer = require("../../config/multer.config");
const swagger_1 = require("@nestjs/swagger");
const upload_service_1 = require("./upload.service");
const passport_1 = require("@nestjs/passport");
let UploadController = class UploadController {
    constructor(uploadService) {
        this.uploadService = uploadService;
    }
    async uploadCompanyImage(file) {
        return file;
    }
    async uploadCompanyAdvantagesImage(file) {
        return file;
    }
    async uploadOrganizationImage(file) {
        return file;
    }
    async uploadServiceImage(file) {
        return file;
    }
    async uploadPhotoAlbumImage(file) {
        return file;
    }
    async uploadBlogImage(file) {
        return file;
    }
};
__decorate([
    common_1.Post('/company/image'),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('image', multer.CompanyOptions)),
    __param(0, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UploadController.prototype, "uploadCompanyImage", null);
__decorate([
    common_1.Post('/company-advantages/image'),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('image', multer.CompanyAdvantagesOptions)),
    __param(0, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UploadController.prototype, "uploadCompanyAdvantagesImage", null);
__decorate([
    common_1.Post('/organization/image'),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('image', multer.OrganizationOptions)),
    __param(0, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UploadController.prototype, "uploadOrganizationImage", null);
__decorate([
    common_1.Post('/service/image'),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('image', multer.ServiceOptions)),
    __param(0, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UploadController.prototype, "uploadServiceImage", null);
__decorate([
    common_1.Post('/photo-album/image'),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('image', multer.PhotoAlbumOptions)),
    __param(0, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UploadController.prototype, "uploadPhotoAlbumImage", null);
__decorate([
    common_1.Post('/blog/image'),
    common_1.UseInterceptors(platform_express_1.FileInterceptor('image', multer.BlogOptions)),
    __param(0, common_1.UploadedFile()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], UploadController.prototype, "uploadBlogImage", null);
UploadController = __decorate([
    swagger_1.ApiTags('Upload'),
    common_1.Controller('upload'),
    common_1.UseGuards(passport_1.AuthGuard('jwt')),
    __metadata("design:paramtypes", [upload_service_1.UploadService])
], UploadController);
exports.UploadController = UploadController;
//# sourceMappingURL=upload.controller.js.map