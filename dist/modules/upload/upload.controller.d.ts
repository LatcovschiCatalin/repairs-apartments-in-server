import { UploadService } from './upload.service';
export declare class UploadController {
    private uploadService;
    constructor(uploadService: UploadService);
    uploadCompanyImage(file: any): Promise<any>;
    uploadCompanyAdvantagesImage(file: any): Promise<any>;
    uploadOrganizationImage(file: any): Promise<any>;
    uploadServiceImage(file: any): Promise<any>;
    uploadPhotoAlbumImage(file: any): Promise<any>;
    uploadBlogImage(file: any): Promise<any>;
}
