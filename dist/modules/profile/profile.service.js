"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateToken = exports.ProfileService = void 0;
const common_1 = require("@nestjs/common");
const users_service_1 = require("../users/users.service");
const crypto = require("crypto");
const nest_emitter_1 = require("nest-emitter");
const express_1 = require("express");
const config_service_1 = require("../../config/config.service");
const errors_service_1 = require("../../config/errors.service");
let ProfileService = class ProfileService {
    constructor(emitter, usersService, config, error) {
        this.emitter = emitter;
        this.usersService = usersService;
        this.config = config;
        this.error = error;
        this.config = config;
    }
    async activateEmailByToken(email, activationToken) {
        const user = await this.usersService.findOneByEmailForActivation(email, activationToken);
        if (!user) {
            throw new common_1.HttpException(this.error.get('USER_NOT_FOUND'), 400);
        }
        if (user.activated) {
            throw new common_1.HttpException(this.error.get('USER_HAS_ALREADY_ACTIVATED'), 400);
        }
        if (user.activationToken !== activationToken) {
            throw new common_1.HttpException(this.error.get('ACTIVATION_TOKEN_INVALID'), 400);
        }
        await this.usersService.activateUser(user.id);
        return express_1.response.redirect(this.config.get('CLIENT_URL'));
    }
    async resetPasswordInit(email) {
        const user = await this.usersService.findOneByEmail(email);
        if (user) {
            user.passwordResetToken = exports.generateToken();
            user.passwordResetExpire = new Date();
            user.passwordResetExpire.setHours(user.passwordResetExpire.getHours() + 24);
            await this.usersService.findByIdAndUpdate(user._id, user);
            return { success: true };
        }
        else {
            throw new common_1.HttpException(this.error.get('USER_NOT_FOUND'), 400);
        }
    }
    async resetPasswordFinish(dto) {
        const date = new Date();
        const user = await this.usersService.findOneByKeyForPasswordReset(dto.key);
        if (user) {
            if (date < user.passwordResetExpire) {
                user.passwordResetToken = null;
                user.passwordResetExpire = null;
                user.password = dto.newPassword;
                user.save();
                return { success: true };
            }
            else {
                throw new common_1.HttpException(this.error.get('TOKEN_TIME_EXPIRED'), 400);
            }
        }
        else {
            throw new common_1.HttpException(this.error.get('ACTIVATION_TOKEN_INVALID'), 400);
        }
    }
};
ProfileService = __decorate([
    common_1.Injectable(),
    __param(0, nest_emitter_1.InjectEventEmitter()),
    __metadata("design:paramtypes", [Object, users_service_1.UsersService,
        config_service_1.ConfigService,
        errors_service_1.ErrorsService])
], ProfileService);
exports.ProfileService = ProfileService;
const generateToken = () => {
    return crypto.randomBytes(16).toString('hex');
};
exports.generateToken = generateToken;
//# sourceMappingURL=profile.service.js.map