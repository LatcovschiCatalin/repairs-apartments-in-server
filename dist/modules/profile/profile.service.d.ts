import { UsersService } from '../users/users.service';
import { CreateProfileDto } from "./dto/create-profile.dto";
import { EmailEvents } from '../email/email.events';
import { ConfigService } from '../../config/config.service';
import { ErrorsService } from '../../config/errors.service';
export declare class ProfileService {
    private readonly emitter;
    private readonly usersService;
    private config;
    private error;
    constructor(emitter: EmailEvents, usersService: UsersService, config: ConfigService, error: ErrorsService);
    activateEmailByToken(email: string, activationToken: string): Promise<void>;
    resetPasswordInit(email: string): Promise<any>;
    resetPasswordFinish(dto: CreateProfileDto): Promise<any>;
}
export declare const generateToken: () => string;
