"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.EmailModule = void 0;
const common_1 = require("@nestjs/common");
const email_service_1 = require("./email.service");
const config_service_1 = require("../../config/config.service");
const nest_emitter_1 = require("nest-emitter");
const events_1 = require("events");
const errors_service_1 = require("../../config/errors.service");
const config_1 = require("@nestjs/config");
const nestjs_sendgrid_1 = require("@ntegral/nestjs-sendgrid");
let EmailModule = class EmailModule {
};
EmailModule = __decorate([
    common_1.Module({
        imports: [nest_emitter_1.NestEmitterModule.forRoot(new events_1.EventEmitter()), config_1.ConfigModule.forRoot(),
            nestjs_sendgrid_1.SendGridModule.forRoot({
                apiKey: process.env.SENDGRID_API_KEY,
            }),],
        controllers: [],
        providers: [config_service_1.ConfigService, email_service_1.EmailService, errors_service_1.ErrorsService],
    })
], EmailModule);
exports.EmailModule = EmailModule;
//# sourceMappingURL=email.module.js.map