import { BlogService } from './blog.service';
import { BlogSchema } from "../../schemas/blog.schema";
import { CreateBlogDto } from "./dto/create-blog.dto";
export declare class BlogController {
    private readonly blogService;
    constructor(blogService: BlogService);
    findAll(query: any): Promise<BlogSchema[]>;
    findById(id: string): Promise<BlogSchema>;
    create(createProductDto: CreateBlogDto): Promise<BlogSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<BlogSchema>;
    remove(id: string): Promise<BlogSchema>;
}
