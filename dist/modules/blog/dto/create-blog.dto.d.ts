export declare class CreateBlogDto {
    title: string;
    slug: string;
    miniDescription: string;
    description: string;
    mainProductImage: object;
}
