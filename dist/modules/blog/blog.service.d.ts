import { CreateBlogDto } from "./dto/create-blog.dto";
import { PaginateModel } from 'mongoose-paginate-v2';
import { BlogSchema } from "../../schemas/blog.schema";
export declare class BlogService {
    private model;
    constructor(model: PaginateModel<BlogSchema>);
    findById(id: string): Promise<BlogSchema>;
    create(createProductDto: CreateBlogDto): Promise<BlogSchema>;
    findByIdAndUpdate(id: string, product: any): Promise<BlogSchema>;
    findAll(queryParams: any): Promise<BlogSchema[]>;
    remove(id: string): Promise<BlogSchema>;
}
