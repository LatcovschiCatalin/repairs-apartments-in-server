export declare class AbstractPaginationQuery {
    readonly page: number;
    readonly limit: number;
}
