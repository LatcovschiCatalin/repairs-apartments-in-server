"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ImageProxyQuery = void 0;
class ImageProxyQuery {
    constructor() {
        this.resize = 'auto:400:400';
    }
}
exports.ImageProxyQuery = ImageProxyQuery;
//# sourceMappingURL=ImageProxy.query.js.map