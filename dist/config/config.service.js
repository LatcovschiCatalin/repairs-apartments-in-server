"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigService = void 0;
const dotenv = require("dotenv");
const fs = require("fs");
class ConfigService {
    constructor() {
        if (process.env.NODE_ENV === 'production' || process.env.NODE_ENV === 'staging') {
            this.envConfig = {
                APP_FULL_HOST: process.env.APP_FULL_HOST,
                API_URL: process.env.API_URL,
                DASHBOARD_URL: process.env.DASHBOARD_URL,
                MONGODB_URI: process.env.MONGODB_URI,
                PORT: process.env.PORT,
                JWT_SECRET: process.env.JWT_SECRET,
                ADMIN_EMAIL: process.env.ADMIN_EMAIL,
                ADMIN_PASSWORD: process.env.ADMIN_PASSWORD,
                CLIENT_URL: process.env.CLIENT_URL,
                SENDGRID_API_KEY: process.env.SENDGRID_API_KEY,
                TO_EMAIL: process.env.TO_EMAIL,
                FROM_EMAIL: process.env.FROM_EMAIL
            };
        }
        else {
            this.envConfig = dotenv.parse(fs.readFileSync('.env'));
        }
    }
    get(key) {
        return this.envConfig[key];
    }
}
exports.ConfigService = ConfigService;
//# sourceMappingURL=config.service.js.map