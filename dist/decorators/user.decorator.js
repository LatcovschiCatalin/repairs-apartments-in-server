"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthUser = void 0;
const common_1 = require("@nestjs/common");
exports.AuthUser = common_1.createParamDecorator((data, req) => {
    return req.user;
});
//# sourceMappingURL=user.decorator.js.map