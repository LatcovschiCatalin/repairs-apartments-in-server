import { Document, Schema } from 'mongoose';
export interface BlogSchema extends Document {
    title: string;
    slug: string;
    miniDescription: string;
    description: string;
    mainProductImage: object;
}
export declare const BlogSchema: Schema<Document<any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
