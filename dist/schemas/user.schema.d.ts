import { Document, Schema } from 'mongoose';
export interface User extends Document {
    readonly email: string;
    password: string;
    readonly firstName: string;
    readonly lastName: string;
    readonly description: string;
    isActive: boolean;
    activationToken: string;
    passwordResetExpire: Date;
    passwordResetToken: string;
    role: string;
    hashPassword(password: string): boolean;
}
export declare const UserSchema: Schema<Document<any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
