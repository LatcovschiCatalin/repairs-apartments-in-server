"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanySchema = void 0;
const mongoose_1 = require("mongoose");
const slug = require("mongoose-slug-updater");
const mongoosePaginate = require("mongoose-paginate-v2");
exports.CompanySchema = new mongoose_1.Schema({
    name: { type: String },
    phone: { type: String },
    slug: { type: String },
    slogan: { type: String },
    servicePageDescription: { type: String },
    blogPageDescription: { type: String },
    photoAlbumsPageDescription: { type: String },
    principalColor: { type: String },
    secondaryColor: { type: String },
    mail: { type: String },
    coordinates: { type: String },
    networksDescription: { type: String },
    whatsapp: { type: String },
    whatsappDescription: { type: String },
    vk: { type: String },
    instagram: { type: String },
    facebook: { type: String },
    workingHours: { type: String },
    mainProductImage: {
        description: String,
        path: { type: String, default: null },
        mimetype: { type: String, default: null },
        updatedAt: { type: Date, default: Date.now },
    },
    mainProductImageContacts: {
        description: String,
        path: { type: String, default: null },
        mimetype: { type: String, default: null },
        updatedAt: { type: Date, default: Date.now },
    },
}, {
    minimize: false,
    timestamps: true,
});
exports.CompanySchema.plugin(slug);
exports.CompanySchema.plugin(mongoosePaginate);
//# sourceMappingURL=company.schema.js.map