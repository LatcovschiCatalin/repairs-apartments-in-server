"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PerfectRenovationSchema = void 0;
const mongoose_1 = require("mongoose");
const slug = require("mongoose-slug-updater");
const mongoosePaginate = require("mongoose-paginate-v2");
exports.PerfectRenovationSchema = new mongoose_1.Schema({
    title: { type: String },
    renovation: [],
}, {
    minimize: false,
    timestamps: true,
});
exports.PerfectRenovationSchema.plugin(slug);
exports.PerfectRenovationSchema.plugin(mongoosePaginate);
//# sourceMappingURL=perfect-renovation.schema.js.map