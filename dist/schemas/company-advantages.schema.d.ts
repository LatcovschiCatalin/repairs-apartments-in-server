import { Document, Schema } from 'mongoose';
export interface CompanyAdvantagesSchema extends Document {
    title: string;
    description: string;
    mainProductImage: object;
}
export declare const CompanyAdvantagesSchema: Schema<Document<any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
