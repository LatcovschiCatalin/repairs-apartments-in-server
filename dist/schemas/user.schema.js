"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserSchema = void 0;
const mongoose_1 = require("mongoose");
const bcrypt_1 = require("bcrypt");
const mongoosePaginate = require("mongoose-paginate-v2");
exports.UserSchema = new mongoose_1.Schema({
    firstName: { type: String },
    lastName: { type: String },
    description: { type: String },
    email: { type: String, required: true, unique: true },
    isActive: { type: Boolean, default: true },
    activationToken: { type: String },
    passwordResetToken: { type: String },
    passwordResetExpire: { type: Date },
    password: { type: String, required: true },
    role: { type: String, enum: ['admin', 'user'], default: 'admin' },
}, {
    timestamps: true,
});
exports.UserSchema.pre('save', function (next) {
    const saltRounds = 10;
    const user = this;
    if (this.isModified('password') || this.isNew) {
        const salt = bcrypt_1.genSaltSync(saltRounds);
        user.password = bcrypt_1.hashSync(user.password, salt);
        next();
    }
    else {
        return next();
    }
});
exports.UserSchema.plugin(mongoosePaginate);
//# sourceMappingURL=user.schema.js.map