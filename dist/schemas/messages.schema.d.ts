import { Document, Schema } from 'mongoose';
export interface MessagesSchema extends Document {
    name: string;
    phone: string;
    mail: string;
    message: string;
}
export declare const MessagesSchema: Schema<Document<any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
