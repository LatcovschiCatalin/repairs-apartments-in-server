import { Document, Schema } from 'mongoose';
export interface CompanySchema extends Document {
    name: string;
    slug: string;
    mail: string;
    phone: string;
    coordinates: string;
    networksDescription: string;
    whatsapp: string;
    whatsappDescription: string;
    vk: string;
    instagram?: string;
    facebook?: string;
    workingHours: string;
    principalColor?: string;
    secondaryColor?: string;
    slogan?: string;
    servicePageDescription: string;
    blogPageDescription: string;
    photoAlbumsPageDescription: string;
    mainProductImage: object;
    mainProductImageContacts: object;
}
export declare const CompanySchema: Schema<Document<any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
