import { Document, Schema } from 'mongoose';
export interface GoalsSchema extends Document {
    title: string;
    goals: object;
}
export declare const GoalsSchema: Schema<Document<any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
