import { Document, Schema } from 'mongoose';
export interface OrganizationSchema extends Document {
    title: string;
    organizations: object;
    description: string;
}
export declare const OrganizationSchema: Schema<Document<any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
