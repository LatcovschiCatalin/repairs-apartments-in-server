"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MessagesSchema = void 0;
const mongoose_1 = require("mongoose");
const slug = require("mongoose-slug-updater");
const mongoosePaginate = require("mongoose-paginate-v2");
exports.MessagesSchema = new mongoose_1.Schema({
    name: { type: String },
    phone: { type: String },
    mail: { type: String },
    message: { type: String },
}, {
    minimize: false,
    timestamps: true,
});
exports.MessagesSchema.plugin(slug);
exports.MessagesSchema.plugin(mongoosePaginate);
//# sourceMappingURL=messages.schema.js.map