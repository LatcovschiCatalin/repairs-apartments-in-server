import { Document, Schema } from 'mongoose';
export interface PhotoAlbumsSchema extends Document {
    title: string;
    albums: object;
}
export declare const PhotoAlbumsSchema: Schema<Document<any, any>, import("mongoose").Model<any, any, any>, undefined, any>;
