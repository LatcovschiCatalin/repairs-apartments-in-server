"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrganizationSchema = void 0;
const mongoose_1 = require("mongoose");
const slug = require("mongoose-slug-updater");
const mongoosePaginate = require("mongoose-paginate-v2");
exports.OrganizationSchema = new mongoose_1.Schema({
    title: { type: String },
    organizations: [],
    description: { type: String },
}, {
    minimize: false,
    timestamps: true,
});
exports.OrganizationSchema.plugin(slug);
exports.OrganizationSchema.plugin(mongoosePaginate);
//# sourceMappingURL=organization.schema.js.map