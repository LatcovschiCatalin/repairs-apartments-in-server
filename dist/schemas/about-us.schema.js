"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AboutUsSchema = void 0;
const mongoose_1 = require("mongoose");
const slug = require("mongoose-slug-updater");
const mongoosePaginate = require("mongoose-paginate-v2");
exports.AboutUsSchema = new mongoose_1.Schema({
    miniDescription: { type: String },
    description: { type: String },
}, {
    minimize: false,
    timestamps: true,
});
exports.AboutUsSchema.plugin(slug);
exports.AboutUsSchema.plugin(mongoosePaginate);
//# sourceMappingURL=about-us.schema.js.map