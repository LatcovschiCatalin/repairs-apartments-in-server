"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BlogSchema = void 0;
const mongoose_1 = require("mongoose");
const slug = require("mongoose-slug-updater");
const mongoosePaginate = require("mongoose-paginate-v2");
exports.BlogSchema = new mongoose_1.Schema({
    title: { type: String },
    slug: { type: String },
    miniDescription: { type: String },
    description: { type: String },
    mainProductImage: {
        description: String,
        path: { type: String, default: null },
        mimetype: { type: String, default: null },
        updatedAt: { type: Date, default: Date.now },
    },
}, {
    minimize: false,
    timestamps: true,
});
exports.BlogSchema.plugin(slug);
exports.BlogSchema.plugin(mongoosePaginate);
//# sourceMappingURL=blog.schema.js.map