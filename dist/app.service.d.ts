import { HttpService } from "@nestjs/axios";
import { ConfigService } from "./config/config.service";
export declare class AppService {
    private httpService;
    private config;
    constructor(httpService: HttpService, config: ConfigService);
    filterImages(path: any, folder: any, company: any, organization: any, album: any): void;
}
