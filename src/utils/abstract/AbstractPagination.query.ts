import {ApiPropertyOptional} from '@nestjs/swagger';

export class AbstractPaginationQuery {
    @ApiPropertyOptional()
    public readonly page: number;
    public readonly limit: number;
}
