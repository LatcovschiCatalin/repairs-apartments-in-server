import {Document, Schema} from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface BlogSchema extends Document {
    title: string;
    slug: string;
    miniDescription: string;
    description: string;
    mainProductImage: object;
}

export const BlogSchema = new Schema(
    {
        title: {type: String},
        slug: {type: String},
        miniDescription: {type: String},
        description: {type: String},
        mainProductImage: {
            description: String,
            path: {type: String, default: null},
            mimetype: {type: String, default: null},
            updatedAt: {type: Date, default: Date.now},
        },
    },
    {
        minimize: false,
        timestamps: true,
    },
);

BlogSchema.plugin(slug);
BlogSchema.plugin(mongoosePaginate);
