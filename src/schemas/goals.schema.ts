import {Document, Schema} from 'mongoose';
import * as slug from 'mongoose-slug-updater';
import * as mongoosePaginate from 'mongoose-paginate-v2';

export interface GoalsSchema extends Document {
    title: string;
    goals: object
}

export const GoalsSchema = new Schema(
    {
        title: {type: String},
        goals: [],
    },
    {
        minimize: false,
        timestamps: true,
    },
);

GoalsSchema.plugin(slug);
GoalsSchema.plugin(mongoosePaginate);
