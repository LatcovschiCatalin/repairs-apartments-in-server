import {Controller, Get, Post, Body, Patch, Param, Delete, Query, UseGuards, Put} from '@nestjs/common';
import {OrganizationService} from './organization.service';
import {CreateOrganizationDto} from './dto/create-organization.dto';
import {ApiBearerAuth, ApiTags} from "@nestjs/swagger";
import {AuthGuard} from "@nestjs/passport";
import {Roles} from "../../decorators/roles.decorator";
import {OrganizationSchema} from "../../schemas/organization.schema";

@ApiTags('Organization')
@Controller('organization')
export class OrganizationController {
    constructor(private readonly service: OrganizationService) {
    }

    @Get()
    async findAll(@Query() query: any): Promise<OrganizationSchema[]> {
        return this.service.findAll(query);
    }

    @Get('/:id')
    async findById(@Param('id') id: string): Promise<OrganizationSchema> {
        return this.service.findById(id);
    }

    @ApiBearerAuth()
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async create(@Body() createProductDto: CreateOrganizationDto) {
        return this.service.create(createProductDto);
    }

    @ApiBearerAuth()
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async findByIdAndUpdate(@Param('id') id: string, @Body() product): Promise<OrganizationSchema> {
        return this.service.findByIdAndUpdate(id, product);
    }

    @ApiBearerAuth()
    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async remove(@Param('id') id: string) {
        return this.service.remove(id);
    }
}
