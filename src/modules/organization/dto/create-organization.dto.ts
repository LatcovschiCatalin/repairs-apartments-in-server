import {ApiProperty} from '@nestjs/swagger';
export class CreateOrganizationDto {
    @ApiProperty()
    title: string;
    @ApiProperty()
    organizations: object;
    @ApiProperty()
    description: string;
}

