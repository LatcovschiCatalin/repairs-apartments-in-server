import {Module} from '@nestjs/common';
import {GoalsService} from './goals.service';
import {GoalsController} from './goals.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {GoalsSchema} from "../../schemas/goals.schema";

@Module({
    imports: [MongooseModule.forFeature([{name: 'Goals', schema: GoalsSchema}])],
    controllers: [GoalsController],
    providers: [GoalsService]
})
export class GoalsModule {
}
