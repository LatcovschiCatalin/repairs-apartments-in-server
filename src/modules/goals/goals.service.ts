import {Injectable} from '@nestjs/common';
import {CreateGoalDto} from './dto/create-goal.dto';
import {InjectModel} from "@nestjs/mongoose";
import {GoalsSchema} from "../../schemas/goals.schema";
import {PaginateModel} from "mongoose-paginate-v2"

@Injectable()
export class GoalsService {
    constructor(@InjectModel('Goals') private model: PaginateModel<GoalsSchema>) {
    }

    async findById(id: string): Promise<GoalsSchema> {
        return await this.model.findById(id).lean();
    }

    async create(createProductDto: CreateGoalDto): Promise<GoalsSchema> {
        return this.model.create(createProductDto);
    }

    async findByIdAndUpdate(id: string, product): Promise<GoalsSchema> {
        return this.model.findByIdAndUpdate(id, product);
    }


    async findAll(queryParams: any): Promise<GoalsSchema[]> {
        const query: any = {};
        const options: any = {
            lean: true,
            page: parseInt(queryParams.page, 10) || 1,
            limit: parseInt(queryParams.limit, 10) || 10,
            sort: {createdAt: -1},
        };

        return this.model.paginate(query, options);
    }

    async remove(id: string): Promise<GoalsSchema> {
        return this.model.findByIdAndDelete(id);
    }
}
