import {ApiProperty} from '@nestjs/swagger';
export class CreateGoalDto {
    @ApiProperty()
    title: string;
    @ApiProperty()
    goals: object;
}

