import { ApiProperty } from '@nestjs/swagger';
import { IsEmail } from 'class-validator';

export class CreateUserDto {
    @ApiProperty()
    readonly firstName: string;
    @ApiProperty()
    readonly lastName: string;
    @ApiProperty()
    readonly role: string;
    @ApiProperty()
    readonly description: string;
    @ApiProperty()
    readonly isActive: boolean;
    @ApiProperty()
    @IsEmail()
    readonly email: string;
    @ApiProperty()
    readonly password: string;
    activationToken: string;
}

