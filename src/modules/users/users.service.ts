import {Injectable} from '@nestjs/common';
import {InjectModel} from '@nestjs/mongoose';
import {CreateUserDto} from './dto/create-user.dto';
import {PaginateModel} from 'mongoose-paginate-v2';
import {User} from '../../schemas/user.schema';
import {UpdateUserDto} from "./dto/update-user.dto";
import {InjectEventEmitter} from 'nest-emitter';
import {EmailEvents} from '../email/email.events';
import * as crypto from 'crypto';

@Injectable()
export class UsersService {
    constructor(
        @InjectModel('User') private userModel: PaginateModel<User>,
        @InjectEventEmitter() private readonly emitter: EmailEvents,
    ) {
    }

    async getMyProfile(id: string): Promise<User> {
        return this.userModel.findById(id).select('-password -passwordResetToken -passwordResetExpire -activationToken');
    }

    async findAll(queryParams): Promise<User[]> {
        const query: any = {};
        const options: any = {
            page: parseInt(queryParams.page, 10) || 1,
            limit: parseInt(queryParams.limit, 10) || 10,
            select: '-password',
        };

        if (queryParams._sort) {
            options.sort = {
                [queryParams._sort]: queryParams._order === 'ASC' ? 1 : -1,
            };
        }

        if (Object.keys(queryParams).length === 0) {
            return this.userModel.find({}).select('-password');
        }

        if (queryParams.email) {
            query.email = {$regex: queryParams.email, $options: 'i'};
        }
        return this.userModel.paginate(query, options);
    }

    async findById(id: string): Promise<User> {
        return this.userModel.findById(id).select('-password');
    }

    async findOneByEmail(email: string): Promise<User> {
        return this.userModel.findOne({email});
    }

    async create(user: CreateUserDto): Promise<User> {
        user.activationToken = this.generateToken();
        return await this.userModel.create(user);
    }

    async findByIdAndUpdate(id: string, userData: UpdateUserDto): Promise<User> {
        return this.userModel.findByIdAndUpdate(id, userData, {new: true}).select('-password');
    }

    async findByIdAndDelete(id: string): Promise<User> {
        return this.userModel.findByIdAndDelete(id).select('-password');
    }

    async findOneByKeyForPasswordReset(key) {
        return this.userModel.findOne({passwordResetToken: key}).select('-password');
    }

    async findOneByEmailForActivation(email, activationToken) {
        return this.userModel.findOne({email, activationToken}).select('-password');
    }

    async activateUser(id) {
        return this.userModel.findByIdAndUpdate(id, {$set: {isActive: true, activationToken: null}});
    }



    generateToken = (): string => {
        return crypto.randomBytes(16).toString('hex');
    };
}
