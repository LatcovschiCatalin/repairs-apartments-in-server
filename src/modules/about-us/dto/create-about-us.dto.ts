import {ApiProperty} from '@nestjs/swagger';
export class CreateAboutUsDto {
    @ApiProperty()
    miniDescription: string;
    @ApiProperty()
    description: string;
}

