import {Injectable} from '@nestjs/common';
import {CreateAboutUsDto} from './dto/create-about-us.dto';
import {InjectModel} from "@nestjs/mongoose";
import {AboutUsSchema} from "../../schemas/about-us.schema";
import {PaginateModel} from 'mongoose-paginate-v2';

@Injectable()
export class AboutUsService {
    constructor(@InjectModel('AboutUs') private model: PaginateModel<AboutUsSchema>) {
    }

    async findById(id: string): Promise<AboutUsSchema> {
        return await this.model.findById(id).lean();
    }

    async create(createProductDto: CreateAboutUsDto): Promise<AboutUsSchema> {
        return this.model.create(createProductDto);
    }

    async findByIdAndUpdate(id: string, product): Promise<AboutUsSchema> {
        return this.model.findByIdAndUpdate(id, product);
    }

    async findAll(queryParams: any): Promise<AboutUsSchema[]> {
        const query: any = {};
        const options: any = {
            lean: true,
            page: parseInt(queryParams.page, 10) || 1,
            limit: parseInt(queryParams.limit, 10) || 10,
            sort: {createdAt: -1},
        };


        return this.model.paginate(query, options);
    }

    async remove(id: string): Promise<AboutUsSchema> {
        return this.model.findByIdAndDelete(id);
    }
}
