import { Module } from '@nestjs/common';
import { AboutUsService } from './about-us.service';
import { AboutUsController } from './about-us.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {AboutUsSchema} from "../../schemas/about-us.schema";

@Module({
  imports: [MongooseModule.forFeature([{name: 'AboutUs', schema: AboutUsSchema}])],
  controllers: [AboutUsController],
  providers: [AboutUsService]
})
export class AboutUsModule {}
