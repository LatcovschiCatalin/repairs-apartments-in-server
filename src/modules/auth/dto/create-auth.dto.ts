import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class CreateAuthDto {
    @ApiProperty()
    @IsEmail()
    public readonly email: string;
    @ApiProperty()
    @IsNotEmpty()
    public readonly password: string;
}
