import {Body, Controller, HttpException, HttpStatus, Post, Response} from '@nestjs/common';
import {AuthService, generateToken} from './auth.service';
import {UsersService} from '../users/users.service';
import {BcryptService} from './bcrypt.service';
import {InjectEventEmitter} from 'nest-emitter';
import {EmailEvents} from '../email/email.events';
import {ApiTags} from '@nestjs/swagger';
import {CreateAuthDto} from "./dto/create-auth.dto";
import {ErrorsService} from '../../config/errors.service';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
    constructor(
        @InjectEventEmitter() private readonly emitter: EmailEvents,
        private authService: AuthService,
        private userService: UsersService,
        private bcryptService: BcryptService,
        private error: ErrorsService,
    ) {
    }


    @Post('/login')
    async authenticateDashboard(@Response() res: any, @Body() body: CreateAuthDto) {
        const user = await this.authService.validateUser({email: body.email});
        if (!user) {
            throw new HttpException(this.error.get('USER_NOT_FOUND'), 400);
        }
        if (!user.isActive) {
            throw new HttpException(this.error.get('USER_NOT_ACTIVE'), 400);
        }
        if ((await this.bcryptService.comparePassword(body.password, user.password)) && user.isActive) {
            const token = await this.authService.signIn(user);
            return res.status(HttpStatus.OK).json({auth: true, token});
        }
        throw new HttpException(this.error.get('PASSWORD_INCORRECT'), 400);
    }
}
