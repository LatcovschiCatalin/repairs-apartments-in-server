import { Module } from '@nestjs/common';
import { EmailService } from './email.service';
import { setApiKey } from '@sendgrid/mail';
import { ConfigService } from '../../config/config.service';
import { NestEmitterModule } from 'nest-emitter';
import { EventEmitter } from 'events';
import { ErrorsService } from '../../config/errors.service';
import {ConfigModule} from "@nestjs/config";
import {SendGridModule} from "@ntegral/nestjs-sendgrid";

@Module({
    imports: [NestEmitterModule.forRoot(new EventEmitter()), ConfigModule.forRoot(),
        SendGridModule.forRoot({
            apiKey: process.env.SENDGRID_API_KEY,
        }),],
    controllers: [],
    providers: [ConfigService, EmailService, ErrorsService],
})
export class EmailModule {
}
