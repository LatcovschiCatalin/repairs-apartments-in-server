import { StrictEventEmitter } from 'nest-emitter';
import { EventEmitter } from 'events';

interface AppEvents {
    orderEmail: (cart) => void;
}

export type EmailEvents = StrictEventEmitter<EventEmitter, AppEvents>;
