import {Injectable, Logger} from '@nestjs/common';
import {ConfigService} from '../../config/config.service';
import {InjectSendGrid, SendGridService} from "@ntegral/nestjs-sendgrid";

@Injectable()
export class EmailService {
    public constructor(@InjectSendGrid() private readonly sendGrid: SendGridService,
                       private readonly config: ConfigService) {
    }

    async sendEmail(obj): Promise<void> {
        await this.sendGrid.send({
            to: this.config.get('TO_EMAIL'),
            from: this.config.get('FROM_EMAIL'),
            subject: 'У вас есть новое сообщение!',
            html: `<div>
    <div>
    <label>Имя клиента: </label>
    <strong>${obj.name}</strong>
    </div><br>
    <div>
    <label>Телефонный номер: </label>
    <a href="tel:${obj.phone}">${obj.phone}</a>
    </div><br>
    <div> 
     <div>
    <label>Электронная почта: </label>
    <a href="mailto:${obj.mail}">${obj.mail}</a>
    </div><br>
    <div> 
     <div>
    <label>Сообщение клиента: </label>
    <strong>${obj.message}</strong>
    </div><br>
    <div>`,
        })
    }
}
