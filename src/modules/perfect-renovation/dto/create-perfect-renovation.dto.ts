import {ApiProperty} from '@nestjs/swagger';
export class CreatePerfectRenovationDto {
    @ApiProperty()
    title: string;
    @ApiProperty()
    renovation: object;
}

