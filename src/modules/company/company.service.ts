import {Injectable} from '@nestjs/common';
import {CreateCompanyDto} from './dto/create-company.dto';
import {CompanySchema} from "../../schemas/company.schema";
import {InjectModel} from "@nestjs/mongoose";
import {PaginateModel} from 'mongoose-paginate-v2';


@Injectable()
export class CompanyService {
    constructor(@InjectModel('Company') private model: PaginateModel<CompanySchema>) {
    }


    async findById(id: string): Promise<CompanySchema> {
        return await this.model.findById(id).lean();
    }

    async create(createProductDto: CreateCompanyDto): Promise<CompanySchema> {
        return this.model.create(createProductDto);
    }

    async findByIdAndUpdate(id: string, product): Promise<CompanySchema> {
        return this.model.findByIdAndUpdate(id, product);
    }

    async findAll(queryParams: any): Promise<CompanySchema[]> {
        const query: any = {};
        const options: any = {
            lean: true,
            page: parseInt(queryParams.page, 10) || 1,
            limit: parseInt(queryParams.limit, 10) || 10,
            sort: {createdAt: -1},
        };


        return this.model.paginate(query, options);
    }

    async remove(id: string): Promise<CompanySchema> {
        return this.model.findByIdAndDelete(id);
    }
}

