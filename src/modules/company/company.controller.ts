import {Controller, Get, Post, Body, Patch, Param, Delete, Query, UseGuards, Put} from '@nestjs/common';
import {CompanyService} from './company.service';
import {CreateCompanyDto} from './dto/create-company.dto';
import {ApiBearerAuth, ApiTags} from "@nestjs/swagger";
import {CompanySchema} from '../../schemas/company.schema';
import {AuthGuard} from "@nestjs/passport";
import {Roles} from "../../decorators/roles.decorator";
import {Observable} from "rxjs";

@ApiTags('Company')
@Controller('company')
export class CompanyController {
    constructor(private readonly service: CompanyService) {
    }


    @Get()
    async findAll(@Query() query: any): Promise<CompanySchema[]> {
        return this.service.findAll(query);
    }

    @Get('/:id')
    async findById(@Param('id') id: string): Promise<CompanySchema> {
        return this.service.findById(id);
    }

    @ApiBearerAuth()
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async create(@Body() createProductDto: CreateCompanyDto) {
        return this.service.create(createProductDto);
    }

    @ApiBearerAuth()
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async findByIdAndUpdate(@Param('id') id: string, @Body() product): Promise<CompanySchema> {
        return this.service.findByIdAndUpdate(id, product);
    }

    @ApiBearerAuth()
    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async remove(@Param('id') id: string) {
        return this.service.remove(id);
    }

}
