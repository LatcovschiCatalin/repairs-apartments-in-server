import {Controller, Get, Post, Body, Patch, Param, Delete, Query, UseGuards, Put} from '@nestjs/common';
import {PhotoAlbumsService} from './photo-albums.service';
import {CreatePhotoAlbumDto} from './dto/create-photo-album.dto';
import {ApiBearerAuth, ApiTags} from "@nestjs/swagger";
import {AuthGuard} from "@nestjs/passport";
import {Roles} from "../../decorators/roles.decorator";
import {PhotoAlbumsSchema} from "../../schemas/photo-albums.schema";

@ApiTags('PhotoAlbums')
@Controller('photo-albums')
export class PhotoAlbumsController {
    constructor(private readonly service: PhotoAlbumsService) {
    }

    @Get()
    async findAll(@Query() query: any): Promise<PhotoAlbumsSchema[]> {
        return this.service.findAll(query);
    }

    @Get('/:id')
    async findById(@Param('id') id: string): Promise<PhotoAlbumsSchema> {
        return this.service.findById(id);
    }

    @ApiBearerAuth()
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async create(@Body() createProductDto: CreatePhotoAlbumDto) {
        return this.service.create(createProductDto);
    }

    @ApiBearerAuth()
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async findByIdAndUpdate(@Param('id') id: string, @Body() product): Promise<PhotoAlbumsSchema> {
        return this.service.findByIdAndUpdate(id, product);
    }

    @ApiBearerAuth()
    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async remove(@Param('id') id: string) {
        return this.service.remove(id);
    }
}
