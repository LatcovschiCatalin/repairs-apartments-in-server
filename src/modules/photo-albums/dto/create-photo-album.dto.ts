import {ApiProperty} from '@nestjs/swagger';

export class CreatePhotoAlbumDto {
    @ApiProperty()
    title: string;
    @ApiProperty()
    organizations: object;
}

