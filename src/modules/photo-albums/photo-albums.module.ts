import {Module} from '@nestjs/common';
import {PhotoAlbumsService} from './photo-albums.service';
import {PhotoAlbumsController} from './photo-albums.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {PhotoAlbumsSchema} from "../../schemas/photo-albums.schema";
import {UploadController} from "../upload/upload.controller";
import {UploadService} from "../upload/upload.service";

@Module({
    imports: [MongooseModule.forFeature([{name: 'PhotoAlbums', schema: PhotoAlbumsSchema}])],
    controllers: [PhotoAlbumsController, UploadController],
    providers: [PhotoAlbumsService, UploadService]
})
export class PhotoAlbumsModule {
}
