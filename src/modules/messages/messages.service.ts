import {Injectable} from '@nestjs/common';
import {CreateMessageDto} from './dto/create-message.dto';
import {InjectModel} from "@nestjs/mongoose";
import {PaginateModel} from "mongoose-paginate-v2"
import {MessagesSchema} from "../../schemas/messages.schema";
import {EmailService} from "../email/email.service";
import {InjectEventEmitter} from "nest-emitter/event-emitter.decorator";
import {EmailEvents} from "../email/email.events";

@Injectable()
export class MessagesService {
    constructor(@InjectModel('Messages') private model: PaginateModel<MessagesSchema>,
                private readonly email: EmailService
    ) {
    }

    async findById(id: string): Promise<MessagesSchema> {
        return await this.model.findById(id).lean();
    }

    async create(createProductDto: CreateMessageDto): Promise<MessagesSchema> {


        return this.model.create(createProductDto);


    }

    async sendEmail(createProductDto: CreateMessageDto): Promise<void> {
        return this.email.sendEmail(createProductDto);
    }

    async findByIdAndUpdate(id: string, product): Promise<MessagesSchema> {
        return this.model.findByIdAndUpdate(id, product);
    }


    async findAll(queryParams: any): Promise<MessagesSchema[]> {
        const query: any = {};
        const options: any = {
            lean: true,
            page: parseInt(queryParams.page, 10) || 1,
            limit: parseInt(queryParams.limit, 10) || 10,
            sort: {createdAt: -1},
        };

        return this.model.paginate(query, options);
    }

    async remove(id: string): Promise<MessagesSchema> {
        return this.model.findByIdAndDelete(id);
    }
}
