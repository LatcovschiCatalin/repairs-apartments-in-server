import {Module} from '@nestjs/common';
import {MessagesService} from './messages.service';
import {MessagesController} from './messages.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {MessagesSchema} from "../../schemas/messages.schema";
import {EmailService} from "../email/email.service";

@Module({
    imports: [MongooseModule.forFeature([{name: 'Messages', schema: MessagesSchema}])],
    controllers: [MessagesController],
    providers: [MessagesService, EmailService]
})
export class MessagesModule {
}
