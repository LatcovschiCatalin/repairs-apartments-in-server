import { IsEmail, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class UpdateProfileDto {
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    public email: string;
}
