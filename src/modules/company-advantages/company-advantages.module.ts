import {Module} from '@nestjs/common';
import {CompanyAdvantagesService} from './company-advantages.service';
import {CompanyAdvantagesController} from './company-advantages.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {CompanyAdvantagesSchema} from "../../schemas/company-advantages.schema";
import {UploadController} from "../upload/upload.controller";
import {UploadService} from "../upload/upload.service";

@Module({
    imports: [MongooseModule.forFeature([{name: 'CompanyAdvantages', schema: CompanyAdvantagesSchema}])],
    controllers: [CompanyAdvantagesController, UploadController],
    providers: [CompanyAdvantagesService, UploadService]
})
export class CompanyAdvantagesModule {
}
