import {Injectable} from '@nestjs/common';
import {InjectModel} from "@nestjs/mongoose";
import {PaginateModel} from 'mongoose-paginate-v2';
import {CompanyAdvantagesSchema} from "../../schemas/company-advantages.schema";
import {CreateCompanyAdvantageDto} from "./dto/create-company-advantage.dto";

@Injectable()
export class CompanyAdvantagesService {
    constructor(@InjectModel('CompanyAdvantages') private model: PaginateModel<CompanyAdvantagesSchema>) {
    }

    async findById(id: string): Promise<CompanyAdvantagesSchema> {
        return await this.model.findById(id).lean();
    }

    async create(createProductDto: CreateCompanyAdvantageDto): Promise<CompanyAdvantagesSchema> {
        return this.model.create(createProductDto);
    }

    async findByIdAndUpdate(id: string, product): Promise<CompanyAdvantagesSchema> {
        return this.model.findByIdAndUpdate(id, product);
    }


    async findAll(queryParams: any): Promise<CompanyAdvantagesSchema[]> {
        const query: any = {};
        const options: any = {
            lean: true,
            page: parseInt(queryParams.page, 10) || 1,
            limit: parseInt(queryParams.limit, 10) || 10,
            sort: {createdAt: -1},
        };

        return this.model.paginate(query, options);
    }

    async remove(id: string): Promise<CompanyAdvantagesSchema> {
        return this.model.findByIdAndDelete(id);
    }
}
