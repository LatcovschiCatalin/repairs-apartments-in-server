import {ApiProperty} from '@nestjs/swagger';
export class CreateCompanyAdvantageDto {
    @ApiProperty()
    title: string;
    @ApiProperty()
    description: string;
    @ApiProperty()
    mainProductImage: object;
}

