import {Controller, Get, Post, Body, Patch, Param, Delete, Query, UseGuards, Put} from '@nestjs/common';
import {ApiBearerAuth, ApiTags} from "@nestjs/swagger";
import {AuthGuard} from "@nestjs/passport";
import {Roles} from "../../decorators/roles.decorator";
import {CompanyAdvantagesService} from "./company-advantages.service";
import {CompanyAdvantagesSchema} from "../../schemas/company-advantages.schema";
import {CreateCompanyAdvantageDto} from "./dto/create-company-advantage.dto";

@ApiTags('CompanyAdvantages')
@Controller('company-advantages')
export class CompanyAdvantagesController {
    constructor(private readonly service: CompanyAdvantagesService) {
    }

    @Get()
    async findAll(@Query() query: any): Promise<CompanyAdvantagesSchema[]> {
        return this.service.findAll(query);
    }

    @Get('/:id')
    async findById(@Param('id') id: string): Promise<CompanyAdvantagesSchema> {
        return this.service.findById(id);
    }

    @ApiBearerAuth()
    @Post()
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async create(@Body() createProductDto: CreateCompanyAdvantageDto) {
        return this.service.create(createProductDto);
    }

    @ApiBearerAuth()
    @Put(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async findByIdAndUpdate(@Param('id') id: string, @Body() product): Promise<CompanyAdvantagesSchema> {
        return this.service.findByIdAndUpdate(id, product);
    }

    @ApiBearerAuth()
    @Delete(':id')
    @UseGuards(AuthGuard('jwt'))
    @Roles('admin')
    async remove(@Param('id') id: string) {
        return this.service.remove(id);
    }
}
