import {Module} from '@nestjs/common';
import {BlogService} from './blog.service';
import {BlogController} from './blog.controller';
import {MongooseModule} from "@nestjs/mongoose";
import {UploadController} from "../upload/upload.controller";
import {BlogSchema} from "../../schemas/blog.schema";
import {ErrorsService} from "../../config/errors.service";
import {UploadService} from "../upload/upload.service";

@Module({
    imports: [MongooseModule.forFeature([{name: 'Blog', schema: BlogSchema}])],
    controllers: [BlogController, UploadController],
    providers: [BlogService, ErrorsService, UploadService]
})
export class BlogModule {
}
