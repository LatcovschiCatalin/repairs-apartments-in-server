import {Injectable} from '@nestjs/common';
import {CreateBlogDto} from "./dto/create-blog.dto";
import {InjectModel} from "@nestjs/mongoose";
import {PaginateModel} from 'mongoose-paginate-v2';
import {BlogSchema} from "../../schemas/blog.schema";

@Injectable()
export class BlogService {
  constructor(@InjectModel('Blog') private model: PaginateModel<BlogSchema>) {
  }

  async findById(id: string): Promise<BlogSchema> {
    return  await this.model.findById(id).lean();
  }

  async create(createProductDto: CreateBlogDto): Promise<BlogSchema> {
    return this.model.create(createProductDto);
  }

  async findByIdAndUpdate(id: string, product): Promise<BlogSchema> {
    return this.model.findByIdAndUpdate(id, product);
  }


  async findAll(queryParams: any): Promise<BlogSchema[]> {
    const query: any = {};
    const options: any = {
      lean: true,
      page: parseInt(queryParams.page, 10) || 1,
      limit: parseInt(queryParams.limit, 10) || 10,
      sort: {createdAt: -1},
    };

    return this.model.paginate(query, options);
  }

  async remove(id: string): Promise<BlogSchema> {
    return this.model.findByIdAndDelete(id);
  }
}

