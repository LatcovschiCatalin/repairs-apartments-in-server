import {HttpService} from "@nestjs/axios";
import {ConfigService} from "./config/config.service";
import * as fs from 'fs';
import {Injectable} from "@nestjs/common";

@Injectable()
export class AppService {
    constructor(private httpService: HttpService, private config: ConfigService) {

    }

    public filterImages(path, folder, company, organization, album) {
        this.httpService.get(`${this.config.get('API_URL')}/api/${path}`).subscribe(data => {
            let src = [];
            let images = [];
            fs.readdir(`public/${folder}/images`, (err, files) => {
                let toggle = false;

                if (album) {
                    data.data.docs.map(album => {
                        album.albums.map(image => {
                            images.push(image);
                        })

                    })
                }
                src = organization ? data.data.docs[0].organizations : album ? images : data.data.docs;

                files.forEach(file => {

                    if (company) {
                        if (file !== data.data.docs[0].mainProductImage.description && file !== data.data.docs[0].mainProductImageContacts.description) {
                            console.log('Deleted unused image');
                            fs.unlinkSync('public/company/images/' + file)
                        }
                    } else {
                        src.map(image => file === image.mainProductImage.description ? toggle = true : '')

                        if (toggle) {
                            toggle = false;
                        } else {
                            console.log('Deleted unused image');
                            fs.unlinkSync(`public/${folder}/images/${file}`)
                        }
                    }
                })
            });
        });
    }
}
