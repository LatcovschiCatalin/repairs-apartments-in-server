import {extname} from 'path';
import {existsSync, mkdirSync} from 'fs';
import {diskStorage} from 'multer';
import {v4 as uuid} from 'uuid';
import {HttpException, HttpStatus} from '@nestjs/common';

export const multerConfig = {
    CompanyUpload: './public/company/images/',
    CompanyAdvantagesUpload: './public/company-advantages/images/',
    OrganizationUpload: './public/organization/images/',
    ServiceUpload: './public/service/images/',
    PhotoAlbumUpload: './public/photo-album/images/',
    BlogUpload: './public/blog/images/',
};

export const fileSizeLimits = {
    images: 10 * 1024 * 1024,
    documents: 25 * 1024 * 1024,
};

export const CompanyOptions = {
    limits: {
        fileSize: fileSizeLimits.images,
    },
    fileFilter: (req: any, file: any, cb: any) => {
        if (file.mimetype.match(/\/(jpeg|png|gif|svg\+xml|x-icon|webp)$/)) {
            cb(null, true);
        } else {
            cb(new HttpException(`Unsupported file type ${extname(file.originalname)}`, HttpStatus.BAD_REQUEST), false);
        }
    },
    storage: diskStorage({
        destination: (req: any, file: any, cb: any) => {
            const uploadPath = multerConfig.CompanyUpload;
            if (!existsSync(uploadPath)) {
                mkdirSync(uploadPath, {recursive: true});
            }
            cb(null, uploadPath);
        },
        filename: (req: any, file: any, cb: any) => {
            cb(null, `${uuid()}${extname(file.originalname)}`);
        },
    }),
};

export const CompanyAdvantagesOptions = {
    limits: {
        fileSize: fileSizeLimits.images,
    },
    fileFilter: (req: any, file: any, cb: any) => {
        if (file.mimetype.match(/\/(jpeg|png|gif|svg\+xml|x-icon|webp)$/)) {
            cb(null, true);
        } else {
            cb(new HttpException(`Unsupported file type ${extname(file.originalname)}`, HttpStatus.BAD_REQUEST), false);
        }
    },
    storage: diskStorage({
        destination: (req: any, file: any, cb: any) => {
            const uploadPath = multerConfig.CompanyAdvantagesUpload;
            if (!existsSync(uploadPath)) {
                mkdirSync(uploadPath, {recursive: true});
            }
            cb(null, uploadPath);
        },
        filename: (req: any, file: any, cb: any) => {
            cb(null, `${uuid()}${extname(file.originalname)}`);
        },
    }),
};
export const OrganizationOptions = {
    limits: {
        fileSize: fileSizeLimits.images,
    },
    fileFilter: (req: any, file: any, cb: any) => {
        if (file.mimetype.match(/\/(jpeg|png|gif|svg\+xml|x-icon|webp)$/)) {
            cb(null, true);
        } else {
            cb(new HttpException(`Unsupported file type ${extname(file.originalname)}`, HttpStatus.BAD_REQUEST), false);
        }
    },
    storage: diskStorage({
        destination: (req: any, file: any, cb: any) => {
            const uploadPath = multerConfig.OrganizationUpload;
            if (!existsSync(uploadPath)) {
                mkdirSync(uploadPath, {recursive: true});
            }
            cb(null, uploadPath);
        },
        filename: (req: any, file: any, cb: any) => {
            cb(null, `${uuid()}${extname(file.originalname)}`);
        },
    }),
};


export const ServiceOptions = {
    limits: {
        fileSize: fileSizeLimits.images,
    },
    fileFilter: (req: any, file: any, cb: any) => {
        if (file.mimetype.match(/\/(jpeg|png|gif|svg\+xml|x-icon|webp)$/)) {
            cb(null, true);
        } else {
            cb(new HttpException(`Unsupported file type ${extname(file.originalname)}`, HttpStatus.BAD_REQUEST), false);
        }
    },
    storage: diskStorage({
        destination: (req: any, file: any, cb: any) => {
            const uploadPath = multerConfig.ServiceUpload;
            if (!existsSync(uploadPath)) {
                mkdirSync(uploadPath, {recursive: true});
            }
            cb(null, uploadPath);
        },
        filename: (req: any, file: any, cb: any) => {
            cb(null, `${uuid()}${extname(file.originalname)}`);
        },
    }),
};

export const PhotoAlbumOptions = {
    limits: {
        fileSize: fileSizeLimits.images,
    },
    fileFilter: (req: any, file: any, cb: any) => {
        if (file.mimetype.match(/\/(jpeg|png|gif|svg\+xml|x-icon|webp)$/)) {
            cb(null, true);
        } else {
            cb(new HttpException(`Unsupported file type ${extname(file.originalname)}`, HttpStatus.BAD_REQUEST), false);
        }
    },
    storage: diskStorage({
        destination: (req: any, file: any, cb: any) => {
            const uploadPath = multerConfig.PhotoAlbumUpload;
            if (!existsSync(uploadPath)) {
                mkdirSync(uploadPath, {recursive: true});
            }
            cb(null, uploadPath);
        },
        filename: (req: any, file: any, cb: any) => {
            cb(null, `${uuid()}${extname(file.originalname)}`);
        },
    }),
};


export const BlogOptions = {
    limits: {
        fileSize: fileSizeLimits.images,
    },
    fileFilter: (req: any, file: any, cb: any) => {
        if (file.mimetype.match(/\/(jpeg|png|gif|svg\+xml|x-icon|webp)$/)) {
            cb(null, true);
        } else {
            cb(new HttpException(`Unsupported file type ${extname(file.originalname)}`, HttpStatus.BAD_REQUEST), false);
        }
    },
    storage: diskStorage({
        destination: (req: any, file: any, cb: any) => {
            const uploadPath = multerConfig.BlogUpload;
            if (!existsSync(uploadPath)) {
                mkdirSync(uploadPath, {recursive: true});
            }
            cb(null, uploadPath);
        },
        filename: (req: any, file: any, cb: any) => {
            cb(null, `${uuid()}${extname(file.originalname)}`);
        },
    }),
};

