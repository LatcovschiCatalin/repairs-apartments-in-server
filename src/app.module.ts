import {Module} from '@nestjs/common';
import {AppController} from './app.controller';
import {DatabaseModule} from './database/database.module';
import {ConfigModule} from '@nestjs/config';
import {MongooseModule} from '@nestjs/mongoose';
import {AuthModule} from './modules/auth/auth.module';
import {UsersModule} from './modules/users/users.module';
import {ConfigService} from './config/config.service';
import {UserSchema} from './schemas/user.schema';
import {EmailModule} from './modules/email/email.module';
import {APP_GUARD} from '@nestjs/core';
import {RolesGuard} from './modules/auth/roles.guard';
import {ProfileModule} from './modules/profile/profile.module';
import {InitService} from "./middlewares/init.service";
import {CompanyModule} from './modules/company/company.module';
import {ServicesModule} from './modules/services/services.module';
import {BlogModule} from './modules/blog/blog.module';
import {AboutUsModule} from './modules/about-us/about-us.module';
import {CompanyAdvantagesModule} from './modules/company-advantages/company-advantages.module';
import {PerfectRenovationModule} from './modules/perfect-renovation/perfect-renovation.module';
import {OrganizationModule} from './modules/organization/organization.module';
import {GoalsModule} from './modules/goals/goals.module';
import {PhotoAlbumsModule} from './modules/photo-albums/photo-albums.module';
import {MessagesModule} from './modules/messages/messages.module';
import {ScheduleModule} from '@nestjs/schedule';
import {AppService} from "./app.service";
import {HttpModule} from '@nestjs/axios';

@Module({

    imports: [
        ConfigModule.forRoot({isGlobal: true}),
        MongooseModule.forFeature([{name: 'User', schema: UserSchema}]),
        AuthModule,
        UsersModule,
        ConfigModule,
        DatabaseModule,
        EmailModule,
        ProfileModule,
        CompanyModule,
        ServicesModule,
        BlogModule,
        AboutUsModule,
        CompanyAdvantagesModule,
        PerfectRenovationModule,
        OrganizationModule,
        GoalsModule,
        PhotoAlbumsModule,
        MessagesModule,
        ScheduleModule.forRoot(),
        HttpModule
    ],
    controllers: [AppController],

    providers: [
        AppService,
        ConfigService,
        InitService,
        {
            provide: APP_GUARD,
            useClass: RolesGuard,
        },
    ],
})
export class AppModule {
}
